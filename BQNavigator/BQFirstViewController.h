//
//  BQFirstViewController.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BQNavigator.h"
#import "BQSelector.h"

@interface BQFirstViewController : UIViewController<BQNavigatable, BQSelectable>

@end
