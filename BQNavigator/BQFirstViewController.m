//
//  BQFirstViewController.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQFirstViewController.h"

@interface BQFirstViewController ()

@end

@implementation BQFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -

- (IBAction)goToSecondVC:(id)sender
{
    [[BQNavigator sharedInstance] openEvent:@"second" animated:YES];
}

- (IBAction)runSomeThing:(id)sender
{
    NSString *result = [[BQSelector sharedInstance] openEvent:@"first" param:@{@"msg":@"test"}];
    
    NSLog(@"result = %@", result);
}

- (NSString *)runRunRun:(NSString *)str
{
    NSLog(@"first VC get %@", str);
    return str;
}

#pragma mark -

- (instancetype)initWithEvent:(NSString *)event
{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        NSLog(@"first init with event:%@", event);
        
        [[BQSelector sharedInstance] registerTarget:self selector:@selector(runRunRun:) toEvent:event];
    }
    return self;
}

- (void)openWithParam:(NSDictionary *)param
{
    NSLog(@"openWithParam first VC get %@", param);
}

- (void)selectWithParam:(id)param
{
    NSLog(@"selectWithParam first VC get %@", param);
}

@end
