//
//  BQSecondViewController.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQSecondViewController.h"
#import "BQSelector.h"

@interface BQSecondViewController ()

@end

@implementation BQSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -

- (IBAction)goToFirstVC:(id)sender
{
    [[BQNavigator sharedInstance] openEvent:@"first" animated:YES];
}

- (IBAction)runSomeThing:(id)sender
{
    NSString *result = [[BQSelector sharedInstance] openEvent:@"second" param:@{@"msg":@"test1"}];
    
    NSLog(@"s result = %@", result);
}

- (NSString *)runRunRun:(NSString *)str
{
    NSLog(@"second VC get %@", str);
    return str;
}

#pragma mark -

- (instancetype)initWithEvent:(NSString *)event
{
    self = [self initWithNibName:nil bundle:nil];
    if (self) {
        NSLog(@"second init with event:%@", event);
        
        [[BQSelector sharedInstance] registerTarget:self selector:@selector(runRunRun:) toEvent:event];
    }
    return self;
}

- (void)openWithParam:(NSDictionary *)param
{
    NSLog(@"openWithParam second VC get %@", param);
}

- (void)selectWithParam:(id)param
{
    NSLog(@"selectWithParam second VC get %@", param);
}

@end
