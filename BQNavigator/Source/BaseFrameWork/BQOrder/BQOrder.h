//
//  BQOrder.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BQOrder : NSObject

@property (nonatomic, readonly) NSString *event;

+ (instancetype)orderWithEvent:(NSString *)event;

@end
