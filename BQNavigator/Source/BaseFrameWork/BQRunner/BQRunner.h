//
//  BQRunner.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BQOrder.h"

typedef void(^BQRunnerCallback)();

@interface BQRunner : NSObject

- (id)runOrder:(BQOrder *)order withOption:(id)option;

@end
