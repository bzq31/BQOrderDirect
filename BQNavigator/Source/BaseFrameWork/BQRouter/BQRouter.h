//
//  BQRouter.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BQOrder.h"
#import "BQRunner.h"

@interface BQRouter : NSObject

// Register API

- (void)registerOrder:(BQOrder *)order toRunner:(BQRunner *)runner;

- (void)mapOrder:(BQOrder *)first toOrder:(BQOrder *)second;    // Second order must have been registed

// Run API

- (id)executeOrder:(BQOrder *)order;

- (id)executeOrderEvent:(NSString *)event;

- (id)executeOrder:(BQOrder *)order withOption:(id)option;

- (id)executeOrderEvent:(NSString *)event withOption:(id)option;

// Query API

- (BQOrder *)registeredOrderByEvent:(NSString *)event;

- (BQRunner *)registeredRunnerByEvent:(NSString *)event;

@end
