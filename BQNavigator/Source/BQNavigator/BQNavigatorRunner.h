//
//  BQNavigatorRunner.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQRunner.h"
#import "BQNavigatorOrder.h"

@interface BQNavigatorRunnerOption : NSObject

@property (nonatomic, assign) BOOL animated;
@property (nonatomic, strong) UIViewController *carrier;
@property (nonatomic, strong) NSDictionary *param;

+ (instancetype)optionWithCarrier:(UIViewController *)carrier param:(NSDictionary *)param animated:(BOOL)animated;

@end

@interface BQNavigatorRunner : BQRunner

@end
