//
//  BQSelectorOrder.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQSelectorOrder.h"

@implementation BQSelectorOrder

+ (instancetype)orderWithEvent:(NSString *)event target:(id<BQSelectable>)target selectorName:(NSString *)selectorName
{
    BQSelectorOrder *order = [self orderWithEvent:event];
    order.selectorName = selectorName;
    order.target = target;
    
    return order;
}

@end
