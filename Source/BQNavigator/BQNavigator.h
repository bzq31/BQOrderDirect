//
//  BQNavigator.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-1.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BQNavigatorOrder.h"
#import "BQNavigatorRunner.h"
#import "BQRouter.h"

@class BQNavigator;

@protocol BQNavigatorDelegate <NSObject>

- (BOOL)navigator:(BQNavigator *)navigator shouldOpen:(NSString *)event withParam:(NSDictionary *)param;

- (void)navigator:(BQNavigator *)navigator willOpen:(NSString *)event withParam:(NSDictionary *)param;

- (void)navigator:(BQNavigator *)navigator didOpen:(NSString *)event withParam:(NSDictionary *)param;

@end

@interface BQNavigator : NSObject

@property (nonatomic, weak) id<BQNavigatorDelegate> delegate;

+ (BQNavigator *)sharedInstance;

// Register API

- (void)registerEntryPlist:(NSString *)filePath;

- (void)registerClass:(Class)cls toEvent:(NSString *)event;

- (void)registerClass:(Class)cls toEvent:(NSString *)event option:(BQNavigatorOrderType)type;

- (void)registerObject:(UIViewController<BQNavigatable> *)object toEvent:(NSString *)event;

- (void)registerObject:(UIViewController<BQNavigatable> *)object toEvent:(NSString *)event option:(BQNavigatorOrderType)type;

- (void)registerClassName:(NSString *)clsName toEvent:(NSString *)event;

- (void)registerClassName:(NSString *)clsName toEvent:(NSString *)event option:(BQNavigatorOrderType)type;

// Map API

- (void)mapEvent:(NSString *)first toEvent:(NSString *)second;  // Second event must have been registed

// Open API

- (void)openEvent:(NSString *)event animated:(BOOL)animated;

- (void)openEvent:(NSString *)event withParam:(NSDictionary *)param animated:(BOOL)animated;

- (void)openEvent:(NSString *)event withParam:(NSDictionary *)param inCarrier:(UIViewController *)ctl animated:(BOOL)animated;

@end
