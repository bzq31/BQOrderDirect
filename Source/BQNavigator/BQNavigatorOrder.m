//
//  BQNavigatorOrder.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQNavigatorOrder.h"

@implementation BQNavigatorOrder

+ (instancetype)orderWithEvent:(NSString *)event className:(NSString *)className type:(BQNavigatorOrderType)type
{
    BQNavigatorOrder *order = [self orderWithEvent:event];
    order.className = className;
    order.type = type;
    
    return order;
}

@end
