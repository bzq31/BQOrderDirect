//
//  BQNavigatorOrder.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BQOrder.h"

typedef enum _BQNavigatorOrderType {
    BQNavigatorOrderTypeMultiton    = 0,   // default
    BQNavigatorOrderTypeSingleton
}BQNavigatorOrderType;

@protocol BQNavigatable <NSObject>

- (instancetype)initWithEvent:(NSString *)event;

@optional

- (void)openWithParam:(NSDictionary *)param;

@end

@interface BQNavigatorOrder : BQOrder

@property (nonatomic, strong) NSString *className;
@property (nonatomic, assign) BQNavigatorOrderType type;

@property (nonatomic, strong) UIViewController<BQNavigatable> *lastObject;

+ (instancetype)orderWithEvent:(NSString *)event
                     className:(NSString *)className
                          type:(BQNavigatorOrderType)type;

@end
