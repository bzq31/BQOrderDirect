//
//  BQOrder.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQOrder.h"

@interface BQOrder ()

@property (nonatomic, strong) NSString *event;

@end

@implementation BQOrder

+ (instancetype)orderWithEvent:(NSString *)event
{
    BQOrder *order = [[self alloc] init];
    order.event = event;
    
    return order;
}

@end
