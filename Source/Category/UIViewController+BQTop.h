//
//  UIViewController+BQTop.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-1.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BQTop)

+ (UIViewController *)topViewController;

+ (UINavigationController *)topNavigationController;

@end
