//
//  BQSelector.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BQSelectorOrder.h"
#import "BQSelectorRunner.h"

@interface BQSelector : NSObject

+ (BQSelector *)sharedInstance;

// Register API

- (void)registerTarget:(id<BQSelectable>)target selectorName:(NSString *)selName toEvent:(NSString *)event;

- (void)registerTarget:(id<BQSelectable>)target selector:(SEL)sel toEvent:(NSString *)event;

// Map API

- (void)mapEvent:(NSString *)first toEvent:(NSString *)second;  // Second event must have been registed

// Open API

- (id)openEvent:(NSString *)event param:(id)param;

@end
