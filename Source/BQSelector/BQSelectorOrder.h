//
//  BQSelectorOrder.h
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQOrder.h"

@protocol BQSelectable <NSObject>

- (void)selectWithParam:(id)param;

@end

@interface BQSelectorOrder : BQOrder

@property (nonatomic, strong) NSString *selectorName;

@property (nonatomic, strong) id<BQSelectable> target;

+ (instancetype)orderWithEvent:(NSString *)event target:(id<BQSelectable>)target selectorName:(NSString *)selectorName;

@end
