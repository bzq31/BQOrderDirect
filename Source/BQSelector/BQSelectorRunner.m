//
//  BQSelectorRunner.m
//  BQNavigator
//
//  Created by Zhiqiang Bao on 15-4-3.
//  Copyright (c) 2015年 Zhiqiang Bao. All rights reserved.
//

#import "BQSelectorRunner.h"
#import "BQSelectorOrder.h"
#import <objc/runtime.h>

@implementation BQSelectorRunner

- (id)runOrder:(BQSelectorOrder *)order withOption:(id)option
{
    id result = [super runOrder:order withOption:option];
    
    NSString *selectName = order.selectorName;
    SEL sel = NSSelectorFromString(selectName);
    id<BQSelectable> target = order.target;
    
    if (target && sel && [target respondsToSelector:sel]) {
        
        if ([target respondsToSelector:@selector(selectWithParam:)]) {
            [target selectWithParam:option];
        }
        
        IMP imp = class_getMethodImplementation([target class], sel);
        
        if (imp) {
            id (*func)(id, SEL, id) = (void *)imp;
            result = func(target, sel, option);
        }
    }
    return result;
}

@end
