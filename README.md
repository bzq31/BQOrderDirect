#BQOrderDirect
###用字符串或URL的方式执行任务的框架，内部实现类似320的导航，同时也实现了基于命令的方法调用。框架可以通过继承扩展。该框架可以用于支持模块去耦合。

======
####BQNavigator

**支持通过命令方式的导航跳转**

注册多例打开方式：

```objc
[[BQNavigator sharedInstance] registerClassName:@"BQFirstViewController" toEvent:@"first" option:BQNavigatorOrderTypeMultiton];
```
注册单例打开方式：

```objc
[[BQNavigator sharedInstance] registerClassName:@"BQFirstViewController" toEvent:@"first" option:BQNavigatorOrderTypeSingleton];
```
打开对应事件：

```objc
[[BQNavigator sharedInstance] openEvent:@"first" animated:YES];
```


======
####BQSelector

**支持通过命令的方式执行selector**

注册事件到指定的object及selector

```objc
[[BQSelector sharedInstance] registerTarget:self selector:@selector(runRunRun:) toEvent:event];
```

打开对应事件：

```objc
NSString *result = [[BQSelector sharedInstance] openEvent:@"first" param:@{@"msg":@"test"}];
```

======
####DEMO
![orderdirect](http://git.oschina.net/bzq31/BQOrderDirect/raw/master/Navigator.gif)
